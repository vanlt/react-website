import React from 'react';
import {
    BrowserRouter as Router,
    Route,
    Link
} from 'react-router-dom';
import RouterHeader from '../router/router';

class Header extends React.Component {
    render() {
        return (
            <React.Fragment>
                <RouterHeader/>
            </React.Fragment>
        );
    }

}

export default Header;
