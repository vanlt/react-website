import React from 'react';
import './Item.css';

class ItemProd extends React.Component {
    constructor(props){
        super(props);
      }
    render() {
        return (
            <React.Fragment>
                  <div className="col-xs-3 col-sm-3 col-md-3 col-lg-3">
                      <div className="thumbnail">
                          <img className="img" src={this.props.items.image} alt=""/>
                          <div className="caption">
                              <h3>{this.props.items.name}</h3>
                              <p>
                                {this.props.items.description}
                              </p>
                              <p>
                                  <a href="#" className="btn btn-primary">Action</a>
                                  <a href="#" className="btn btn-default">Action</a>
                              </p>
                          </div>
                      </div>
                  </div>
            </React.Fragment>
        );
    }
}
export default ItemProd;
