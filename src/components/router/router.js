import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';
import Login from '../login/Login';
import Register from '../register/Register';
import Product from '../product/Product';

class RouterHeader extends React.Component{
  render(){
    return (
      <Router>
        <React.Fragment>
          <nav class="navbar navbar-default" role="navigation">
            
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">Home</a>
            </div>
          
           
            <div class="collapse navbar-collapse navbar-ex1-collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="#">Link</a></li>
                <li><Link to ="/product">Product</Link></li>
              </ul>
              <form class="navbar-form navbar-left" role="search">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Search"/>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
              </form>
              <ul class="nav navbar-nav navbar-right">
                <li><Link to ="/login">Login</Link></li>
                <li><Link to ="/register">Register</Link></li>
              </ul>
            </div>
          </nav>
          <Route exact path ="/product" component={Product}/>
          <Route  path ="/login" component={Login}/>
          <Route  path ="/register" component={Register}/> */
          
        {/* <nav className="navbar navbar-default navbar-fixed-top" role="navigation">
            <Link>Home</Link>
        </nav>
        <ul class="nav navbar-nav">
            <li>
                <Link to ="/product">Product</Link>
            </li>
            <li>
                <Link to ="/login">Login</Link>
            </li>
            <li>
                <Link to ="/register">Register</Link>
            </li>
            <li>
                <Link >Register</Link>
            </li>
        </ul>   
            <Route exact path ="/product" component={Product}/>
            <Route  path ="/login" component={Login}/>
            <Route  path ="/register" component={Register}/> */}
       </React.Fragment>
       </Router>
    );
  }
  
}

export default RouterHeader;
 